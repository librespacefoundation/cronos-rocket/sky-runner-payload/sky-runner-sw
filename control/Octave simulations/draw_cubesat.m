
function[]= draw_cubesat(state,rgb)

%% This function helps visualize the motion of the body of the cubesat,
% modeled as a parallelogram. It takes as input the color of the parallelogram and 
%the state vector  x = [phi phi_dot theta theta_dot] and animates the motion of the object. 
%%
phi = state(1); % animate roll
%theta = state(3); % animate pitch
L = 0.1; % width
H = 0.3; %height
center1=L/2;
center2=H/2;
R= ([cos(phi), -sin(phi); sin(phi), cos(phi)]);
%R = ([cos(theta), -sin(theta); sin(theta), cos(theta)]);
X=([-L/2, L/2, L/2, -L/2]);
Y=([-H/2, -H/2, H/2, H/2]);

for i=1:4
T(:,i)=R*[X(i); Y(i)];
end
x_lower_left=center1+T(1,1);
x_lower_right=center1+T(1,2);
x_upper_right=center1+T(1,3);
x_upper_left=center1+T(1,4);
y_lower_left=center2+T(2,1);
y_lower_right=center2+T(2,2);
y_upper_right=center2+T(2,3);
y_upper_left=center2+T(2,4);
x_coor=[x_lower_left x_lower_right x_upper_right x_upper_left];
y_coor=[y_lower_left y_lower_right y_upper_right y_upper_left];
FigureStorage = patch('Vertices',[x_coor; y_coor]','Faces',[1 2 3 4],'Edgecolor',rgb,'Facecolor','none','Linewidth',1.2);
axis equal;
axis([-0.1 .3 -.1 .4]);
set(gcf,'Position',[100 100 1000 400])
drawnow, hold off
delete(FigureStorage);
end

