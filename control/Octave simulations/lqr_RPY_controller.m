% Octave users
% pkg load control

%% Parameters 
m = 0.123;
r = 0.218;
M = 0.415;
R = 0.05;

H = 0.08;
x_M = H/2;
x_m = 0.155;
x_cm = (m*x_m+M*x_M)/(m+M);

Ix = 1/4*m*r^2 + 1/4*M*R^2 + 1/12*M*H^2 + M*(x_M -x_cm)^2 + m*(x_m -x_cm)^2;
Iy = Ix;
Iz = 1/2*m*r^2 + 1/2*M*R^2; 
I_bz = 1/2*M*R^2; % frictionless bearing assumtion


g = 9.89; % gravitational acceleration (m / s^2)
omega_rpm = 1.5820e+03; % speed of gyro (rpm) 
omega = pi / 30 * omega_rpm;

% Descent velocity
%u = 8.2859;


a = (Iy-I_bz)/Ix*omega;
b = (I_bz-Ix)/Iy*omega;
%% Continuous Time Model definition

A = [ 0 0 0 1 0 0; 
      0 0 0 0 1 0;  
      0 0 0 0 0 1; 
      0 0 0 0 a 0; 
      0 0 0 b 0 0; 
      0 0 0 0 0 0; 
      ];

B = [0 0 0;
     0 0 0;
     0 0 0;
     1/Ix 0 0;
     0 1/Iy 0;
     0 0 1/I_bz;];
 
C = eye(6, 6);
D = zeros(6, 3);

%% Controllability and observability 

%controllability = rank(ctrb(A,B)); = 6 -> is controllable 
%observability = rank(obsv(A,C)); = 6 -> is observable

%% State-space Model Definition
states = {'phi', 'theta', 'psi','phi_{dot}', 'theta_{dot}', 'psi_{dot}'};
inputs = {'M_x' 'M_y' 'M_z'};

% State-Space Model
SS = ss(A, B, C, D, 'statename',states,'inputname',inputs,'outputname',states);
% Transfer functions
G = tf(SS);



% Maximum actuation 
M_x_max = 0.0111;
M_y_max = M_x_max;
M_z_max = 0.0136;


phi_x_max = 20 * pi / 180;
phi_y_max = phi_x_max;
phi_z_max = 20 * pi / 180;
 
%% LQR


Q = [1     0     0     0     0     0;
     0     1     0     0     0     0;
     0     0     1000000    0     0     0;
     0     0     0     1     0    0;
     0     0     0     0     1     0;
     0     0     0     0     0     1000000]; 
% The higher the value the more it is penalized

R = [100000     0     0;
     0     100000     0;
     0     0     10000];
% The smaller the value the more aggressive the control


[K, P, e] = lqr(A, B, Q, R);
 
 
% Controlled system
 
Ac = A - B * K;
Bc = zeros(6, 3);
 
 
 SScl = ss(Ac, Bc, C, D,'statename',states,'inputname',inputs,'outputname',states);
 
 t = 0 : 0.001 : 10;
 r = 0.01*ones(3, length(t));
 X0 = [0.01; 0.01; 0.01; 0; 0; 0];
 figure;
lsim(SScl,r',t, X0);
grid on 
[y,t,x]=lsim(SScl,r',t, X0);

 % Feedback Control Law u = - K x
 u_control =  -(K * x')';
 
%  % Plots
%  
% % Outputs / States
% figure(2);
% plot(t,y(:,3),t,y(:,6));
% title('Step response yaw')
% 
% figure(3);
% plot(t,y(:,1),t,y(:,4));
% title('Step response roll')
% figure(4);
% plot(t,y(:,2),t,y(:,5));
% title('Step response pitch')

% Torques
figure(5);
%plot(t,(I_bz-Iy) * u_control(:,1),t,(I_bz-Iy) * u_control(:,2),t,(I_bz-Iy) * u_control(:,3));
plot(t, u_control(:,1),t, u_control(:,2),t, u_control(:,3));

title('Inputs')


