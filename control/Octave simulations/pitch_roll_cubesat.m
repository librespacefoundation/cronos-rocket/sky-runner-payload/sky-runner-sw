function dx = pitch_roll_cubesat(x,Ix,Iy,Iz,Mx,My,Omega)
%This is the handle function used in sim_pitch_roll_cubesat.m by ode45. 
%It describes the dynamics of the system in form of differential equations.

% x = [phi phi_dot theta theta_dot] - state vector 
% dx = [phi_dot phi_ddot theta_dot theta_ddot] - derivative of the state vector 

% The state vector are the angles made with the x and y axis which can also be measured by the IMU. 
% The inputs to the system are the torques Mx, My along the x and y axes.

% Ix, Iy, Iz - moments of inertia 
% Omega - spin rate of the autogyro rotor
% Mx, My - torques Mx, My along the x and y axes
% roll - phi / pitch - theta / yaw - psi 

% closed-loop control equation(will be used later):
% d/dt(x) = A*x +Bu

%phi = x(1);
phi_dot = x(2);
%theta = x(3);
theta_dot = x(4);

% simplified dynamics of pitch and roll angles(Euler Equations of Motion)
dx(1,1) = x(2);
dx(2,1) = (Mx - (Iz-Iy)*theta_dot*Omega)/Ix;
dx(3,1) = x(4);
dx(4,1) = (My - (Ix-Iz)*phi_dot*Omega)/Iy;