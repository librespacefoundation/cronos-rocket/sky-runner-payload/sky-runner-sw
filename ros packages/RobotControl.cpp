#include <ros/ros.h>
#include <iostream>
#include "std_msgs/Float32.h"
#include "std_msgs/Int8.h"
#include "unistd.h"
#include "sensor_msgs/Imu.h"
#include "nav_msgs/Odometry.h"
#define _USE_MATH_DEFINES
#include <cmath>
#include <control/ServoAngles.h>
#include <control/EulerAngles.h>

//TO DO 
// ✓ Get filtered IMU data
// ✓ Calculate moments based on control model
// ✓ Calculate angles from moments from fitted curve equation
// ✓ Convert Quaternions to Euler 



class RobotControl {
public: 

	// ROS Variables
	ros::NodeHandle n;
	ros::Publisher pub_angles;
	ros::Publisher pub_moments;
	ros::Subscriber sub_complementary_imu;
	
	// Messages
	control::ServoAngles angles_msg; 
	sensor_msgs::Imu com_calib_imu_msg;
	std_msgs::Float32 moments_msg;
	control::EulerAngles euler_msg;
	
	// Topics
	std::string angles_topic;
	std::string moments_topic;
	std::string euler_topic;

	// Custom variables
	const int MAX_ANGLE = 35;
	const int MIN_ANGLE = -35;
	const float K[2] = {0.0023,0.0028​};
	struct Quaternion {
    	float w, x, y, z;};
	struct EulerAngles {
    	float roll, pitch, yaw;};



//Constuctor
RobotControl() {
	  n = ros::NodeHandle("~");
	  angles_topic = "/servo_angles";
	  moments_topic = "/moments";
	  euler_topic = "/euler_angles";
	  pub_angles = n.advertise<control::ServoAngles>(n.resolveName(angles_topic), 1000);
	  pub_moments = n.advertise<std_msgs::Float32>(n.resolveName(moments_topic), 1000);
	  pub_euler = n.advertise<control::EulerAngles>(n.resolveName(euler_topic), 1000);
	  sub_complementary_imu = n.subscribe<sensor_msgs::Imu>("imu/data",10, &AvoidObstacle::laserCallback, this);
	  ROS_INFO("Initializing node .................................");
} 
	void calc_angles(const sensor_msgs::Imu::ConstPtr &com_calib_imu_msg);
	EulerAngles QuaternionsToEulerAngles(Quaternion q);
};


void RobotControl::calc_angles(const sensor_msgs::Imu::ConstPtr &com_calib_imu_msg) {

	//Get Quaternions
	x = com_calib_imu_msg->orientation.x;
	y = com_calib_imu_msg->orientation.y;
	z = com_calib_imu_msg->orientation.z;
	w = com_calib_imu_msg->orientation.w;
	// ROS_INFO("Imu Orientation in Quaternion form x: [%f], y: [%f], z: [%f], w: [%f]", msg->orientation.x,msg->orientation.y,msg->orientation.z,msg->orientation.w);

	// Convert Quternions to Euler routine 
	EulerAngles r =ToEulerAngles({x,y,z,w});
	euler_msg.roll = r.roll; 
	euler_msg.pitch = r.pitch; 
	euler_msg.yaw = r.yaw; 
	pub_euler.publish(euler_msg);
	
	//Get rotation around z axis in rad/s
	x1 = r.yaw; 
	
	//Get rotational velocity around z axis in rad/s
	x2 = com_calib_imu_msg->angular_velocity.z;
	
	//Calculate and publish moment based on control model
	Mz = -(K[0]*x1 + K[1]*x2);
	moments_msg.data = Mz;
	pub_moments.publish(moments_msg);
	
	//Calculate and publish actuation angles
	angle = Mz*Mz*Mz*2045562.14 + Mz*39.56;
	angles_msg.angle1 = angle;
	angles_msg.angle2 = angle;
	angles_msg.angle3 = angle;
	angles_msg.angle4 = angle;
	pub_angles.publish(angles_msg);
	//ROS_INFO_STREAM("The angle of the 1st servo is " << angles_msg.angle1 <<", the angle of the 2nd is "<< angles_msg.angle2 <<", the angle of the 3rd is "<< angles_msg.angle3<<" and the angle of the 4th is <<" angles_msg.angle4");
}



// This function assumes normalized quaternion
// Converts to Euler angles in 3-2-1 sequence
EulerAngles RobotControl::QuaternionsToEulerAngles(Quaternion q) {
    EulerAngles angles;

    // roll (x-axis rotation)
    double sinr_cosp = 2 * (q.w * q.x + q.y * q.z);
    double cosr_cosp = 1 - 2 * (q.x * q.x + q.y * q.y);
    angles.roll = std::atan2(sinr_cosp, cosr_cosp);

    // pitch (y-axis rotation)
    double sinp = std::sqrt(1 + 2 * (q.w * q.y - q.x * q.z));
    double cosp = std::sqrt(1 - 2 * (q.w * q.y - q.x * q.z));
    angles.pitch = 2 * std::atan2(sinp, cosp) - M_PI / 2;

    // yaw (z-axis rotation)
    double siny_cosp = 2 * (q.w * q.z + q.x * q.y);
    double cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z);
    angles.yaw = std::atan2(siny_cosp, cosy_cosp);

    return angles;
}

int main(int argc, char** argv) {

    ros::init(argc, argv, "robot_control");
    RobotControl robotControl;
    ros::spin();
    return 0;
}



