#! /usr/bin/env python3
import time
import board
import adafruit_fxos8700
import adafruit_fxas21002c
import rospy
import numpy as np

from sensor_msgs.msg import Imu
from sensor_msgs.msg import MagneticField

class SensorPub():

    def __init__(self):
        self.pub_imu_raw = rospy.Publisher('/imu/data_raw', Imu, queue_size=1)
        self.pub_mag_raw = rospy.Publisher('/mag/data_raw', MagneticField, queue_size=1)
        self.pub_imu_calib = rospy.Publisher('/imu/calib_data', Imu, queue_size=1)
        self.pub_mag_calib = rospy.Publisher('/mag/calib_data', MagneticField, queue_size=1)   
        self.i2c = board.I2C() 
        self.fxas = adafruit_fxas21002c.FXAS21002C(self.i2c)
        self.fxos  = adafruit_fxos8700.FXOS8700(self.i2c)
        self.imu_msg = Imu()
        self.magnetometer_msg = MagneticField()

    def pub_acc_gyro(self):
        accel_x, accel_y, accel_z = self.fxos.accelerometer
        gyro_x,  gyro_y,  gyro_z = self.fxas.gyroscope
        self.imu_msg.header.frame_id = "base_link"
        self.imu_msg.header.stamp = rospy.Time.now()
        self.imu_msg.linear_acceleration.x = accel_x
        self.imu_msg.linear_acceleration.y = accel_y
        self.imu_msg.linear_acceleration.z = accel_z
        self.imu_msg.angular_velocity.x = gyro_x
        self.imu_msg.angular_velocity.y = gyro_y
        self.imu_msg.angular_velocity.z = gyro_z
        self.pub_imu_raw.publish(self.imu_msg)
        rospy.loginfo("Publishing into /imu/data_raw topic!")

    def pub_magnetometer(self):
        mag_x, mag_y, mag_z = self.fxos.magnetometer
        self.magnetometer_msg.header.stamp = rospy.Time.now()
        self.magnetometer_msg.magnetic_field.x = mag_x
        self.magnetometer_msg.magnetic_field.y = mag_y
        self.magnetometer_msg.magnetic_field.z = mag_z
        self.pub_mag_raw.publish(self.magnetometer_msg)
        rospy.loginfo("Publishing into /mag/data_raw topic!")

    def calibrated_mag_data(self):
        mag_x, mag_y, mag_z = self.fxos.magnetometer
        raw_mag = np.array([[mag_x], [mag_y], [mag_z]])
        biases = np.array(np.mat('-13.525692; -35.817735; -31.867910'))
        A_inv = np.array([[1.099265, -0.033624, -0.005563], [-0.033624, 1.086130, 0.028439], [-0.005563, 0.028439, 1.156762]]);
        unbiased_mag = np.subtract(raw_mag, biases)
        calibrated_mag_data = np.matmul(A_inv, unbiased_mag)
        self.magnetometer_msg.header.stamp = rospy.Time.now()
        self.magnetometer_msg.magnetic_field.x = float(calibrated_mag_data[0])
        self.magnetometer_msg.magnetic_field.y = float(calibrated_mag_data[1])
        self.magnetometer_msg.magnetic_field.z = float(calibrated_mag_data[2])
        self.pub_mag_calib.publish(self.magnetometer_msg)
        rospy.loginfo("Publishing into /mag/calib_data topic!")
    
    def calibrated_imu_data(self):
        accel_x, accel_y, accel_z = self.fxos.accelerometer
        gyro_x,  gyro_y,  gyro_z = self.fxas.gyroscope
        raw_acc = np.array([[accel_x], [accel_y], [accel_z]])
        biases_acc = np.array(np.mat('-0.005820; -0.013673; -0.004559'))
        A_inv_acc = np.array([[1.007482, 0.002221, 0.000726], [0.002221, 1.027824, -0.004322], [0.000726, -0.004322, 0.976788]]);
        unbiased_acc = np.subtract(raw_acc, biases_acc)
        calibrated_imu_data = np.matmul(A_inv_acc, unbiased_acc)
        self.imu_msg.header.frame_id = "base_link"
        self.imu_msg.header.stamp = rospy.Time.now()
        self.imu_msg.linear_acceleration.x = float(calibrated_imu_data[0])
        self.imu_msg.linear_acceleration.y = float(calibrated_imu_data[1])
        self.imu_msg.linear_acceleration.z = float(calibrated_imu_data[2])
        self.imu_msg.angular_velocity.x = gyro_x
        self.imu_msg.angular_velocity.y = gyro_y
        self.imu_msg.angular_velocity.z = gyro_z
        self.pub_imu_calib.publish(self.imu_msg)
        rospy.loginfo("Publishing into /imu/calib_data topic!")
        
        
       

if __name__ == '__main__':
    rospy.init_node('imu_node')
    obj = SensorPub()
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        obj.pub_acc_gyro()
        obj.pub_magnetometer()
        obj.calibrated_mag_data()
        obj.calibrated_imu_data()
        rate.sleep()

