#! /usr/bin/env python3
import time
import board
import adafruit_fxos8700
import adafruit_fxas21002c
import rospy

from sensor_msgs.msg import Imu
from sensor_msgs.msg import MagneticField

class SensorPub():

    def __init__(self):
        self.pub1 = rospy.Publisher('/imu', Imu, queue_size=1)
        self.pub2 = rospy.Publisher('/magnetometer', MagneticField, queue_size=1)
        self.i2c = board.I2C() 
        self.fxas = adafruit_fxas21002c.FXAS21002C(self.i2c)
        self.fxos  = adafruit_fxos8700.FXOS8700(self.i2c)
        self.imu_msg = Imu()
        self.magnetometer_msg = MagneticField()

    def pub_acc_gyro(self):
        accel_x, accel_y, accel_z = self.fxos.accelerometer
        gyro_x,  gyro_y,  gyro_z = self.fxas.gyroscope
        self.imu_msg.header.frame_id = "base_link"
        self.imu_msg.header.stamp = rospy.Time.now()
        self.imu_msg.linear_acceleration.x = accel_x
        self.imu_msg.linear_acceleration.y = accel_y
        self.imu_msg.linear_acceleration.z = accel_z
        self.imu_msg.angular_velocity.x = gyro_x
        self.imu_msg.angular_velocity.y = gyro_y
        self.imu_msg.angular_velocity.z = gyro_z
        self.pub1.publish(self.imu_msg)
        rospy.loginfo("Publishing into /imu topic!")

    def pub_magnetometer(self):
       mag_x, mag_y, mag_z = self.fxos.magnetometer
       self.magnetometer_msg.header.stamp = rospy.Time.now()
       self.magnetometer_msg.magnetic_field.x = mag_x
       self.magnetometer_msg.magnetic_field.y = mag_y
       self.magnetometer_msg.magnetic_field.z = mag_z
       self.pub2.publish(self.magnetometer_msg)
       rospy.loginfo("Publishing into /magnetometer topic!")


if __name__ == '__main__':
    rospy.init_node('imu_publisher')
    obj = SensorPub()
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        obj.pub_acc_gyro()
        obj.pub_magnetometer()
        rate.sleep()

