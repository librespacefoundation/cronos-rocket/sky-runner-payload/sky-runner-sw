#include <ros/ros.h>
#include "sensor_msgs/Imu.h"//acc and gyro
#include "sensor_msgs/MagneticField.h"//magnetometer
#include "std_msgs/Float32.h"
#include "std_msgs/Int16.h"
#include "unistd.h"


/* Assign a unique ID to this sensor at the same time */
//Adafruit_FXOS8700 accelmag = Adafruit_FXOS8700(0x8700A, 0x8700B);
class ImuSub {
public:

ros::NodeHandle n;
ros::Subscriber sub_imu;
ros::Subscriber sub_mag;
sensor_msgs::Imu imu_msg;
sensor_msgs::MagneticField mag_msg;


std::string imu_topic;
std::string mag_topic;

float gyr_x, gyr_y, gyr_z;
float acc_x, acc_y, acc_z;
float mag_x, mag_y, mag_z;

ImuSub() {
  n = ros::NodeHandle("~");
  sub_imu = n.subscribe("/imu",10, &ImuSub::imuCallback, this);
  sub_mag = n.subscribe("/magnetometer",10, &ImuSub::magCallback, this);
  ROS_INFO("Initializing node .................................");
} //Constuctor

void imuCallback(const sensor_msgs::Imu::ConstPtr &imu_msg);
void magCallback(const sensor_msgs::MagneticField::ConstPtr &mag_msg);
};

void ImuSub::imuCallback(const sensor_msgs::Imu::ConstPtr &imu_msg) {
gyr_x = imu_msg->angular_velocity.x;
gyr_y = imu_msg->angular_velocity.y;
gyr_z = imu_msg->angular_velocity.z;
acc_x = imu_msg->linear_acceleration.x;
acc_y = imu_msg->linear_acceleration.y;
acc_z = imu_msg->linear_acceleration.z;
ROS_INFO("Gyroscope readings: x - %f, y - %f, z - %f",gyr_x, gyr_y, gyr_z);
ROS_INFO("Accelerometer readings: x - %f, y - %f, z - %f",acc_x, acc_y, acc_z);
}

void ImuSub::magCallback(const sensor_msgs::MagneticField::ConstPtr &mag_msg) {
mag_x = mag_msg->magnetic_field.x;
mag_y = mag_msg->magnetic_field.y;
mag_z = mag_msg->magnetic_field.z;
ROS_INFO("Magnetometer readings: x - %f, y - %f, z - %f",mag_x, mag_y, mag_z);
}

int main(int argc, char** argv) {

    ros::init(argc, argv, "imu_sub");
    ImuSub obj;
    ros::spin();
    return 0;
}

