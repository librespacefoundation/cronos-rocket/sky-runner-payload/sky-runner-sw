#include <servo_control/ServoAngles.h>
#include <ros/ros.h>
#include <iostream>
#include "std_msgs/Float32.h"
#include "std_msgs/Int8.h"
#include "unistd.h"

class ServoAnglePub {
public: 

ros::NodeHandle n;
ros::Publisher pub;
servo_control::ServoAngles servo_msg;
std::string topic;
int servo1, servo2, servo3, servo4; 
int MAX_ANGLE = 40;
int MIN_ANGLE = -40;

ServoAnglePub() {
  n = ros::NodeHandle("~");
  topic = "/servo_angles";
  pub = n.advertise<servo_control::ServoAngles>(n.resolveName(topic), 1000);
  ROS_INFO("Initializing node .................................");
} //Constuctor

void pub_servo_angles() {
    ros::Rate loop_rate(2); 
    servo1 = 0; // get data from control
    servo2 = 10; // get data from control
    servo3 = 10; // get data from control
    servo4 = 10; // get data from control
    servo_msg.angle1 = servo1;
    servo_msg.angle2 = servo2;
    servo_msg.angle3 = servo3;
    servo_msg.angle4 = servo4;
    pub.publish(servo_msg);
   // ROS_INFO_STREAM("The angle of the 1st servo is " << servo_msg.angle1 <<", the angle of the 2nd is "<< servo_msg.angle2 <<", the angle of the 3rd is "<< servo_msg.angle3<<" and the angle of the 4th is <<" servo_msg.angle4");
    ros::spinOnce();
    loop_rate.sleep();
    }
};

int main(int argc, char** argv) {

    ros::init(argc, argv, "servo_angles_pub");
    ServoAnglePub var;
    while (ros::ok())
    {
     var.pub_servo_angles();   
    }
    
    return 0;
}
