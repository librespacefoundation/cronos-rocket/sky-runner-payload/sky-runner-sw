#include <SPI.h>
#include <SFE_BMP180.h>
#include <Wire.h>
#include <math.h>
#include <stdlib.h>


#define SPI_BUF_LEN 8
#define ALTITUDE 1655.0 // random number, temperature will not be affected
#define RPI_RUNS_PIN 5


#define T_INT_POS 0
#define T_FL_POS 1
#define T_Q_POS 2
#define P_INT_POS 3
#define P_FL_POS 4
#define P_Q_POS 5
#define AX_INT_POS 6
#define AX_FL_POS 7
#define AX_Q_POS 8
#define AY_INT_POS 9
#define AY_FL_POS 10
#define AY_Q_POS  11
#define AZ_INT_POS 12
#define AZ_FL_POS 13
#define AZ_Q_POS 14
#define MX_INT_POS 15
#define MX_FL_POS 16
#define MX_Q_POS 17
#define MY_INT_POS 18
#define MY_FL_POS 19
#define MY_Q_POS 20
#define MZ_INT_POS 21
#define MZ_FL_POS 22
#define MZ_Q_POS 23
#define GX_INT_POS 24
#define GX_FL_POS 25
#define GX_Q_POS 26
#define GY_INT_POS 27
#define GY_FL_POS 28
#define GY_Q_POS 29
#define GZ_INT_POS 30
#define GZ_FL_POS 31
#define GZ_Q_POS 32
// sent variables positions
#define ANGLE_1_POS  0
#define ANGLE_2_POS  1
#define ANGLE_3_POS  2
#define ANGLE_4_POS  3


// to handle data on the SPI
volatile byte c;
volatile static byte spi_buf_pos = 0;

byte rpi_runs = 0;

//double T = 85.3 ;
//double P = 1500.2;
//double Ax = -569.8;
//double Ay = -879.4;
//double Az = +2000.7;
//double Gx = 6000;
//double Gy = 12.3;
//double Gz = -5;
//double Mx = 178.9;
//double My = -789.6;
//double Mz = 8976.5;

static double T = 5.7 ;
double P = 6.9;
double Ax = 0;
double Ay = 0;
double Az = 0;
double Gx =0;
double Gy = 0;
double Gz = 0;
double Mx = 0;
double My = 0;
double Mz = 0;


unsigned char temp_byte_2 ;

unsigned char angle_1 = 0;
unsigned char angle_2 = 0;
unsigned char angle_3 = 0;
unsigned char angle_4 = 0;


unsigned long timeold = 0  ; 
unsigned long timenew ; 
const long spi_delay = 30; 


// bmp180 related variables
SFE_BMP180 pressure;
char bmp_temp_status;
//double T;


void setup() {
    Serial.begin(9600);
    pinMode(RPI_RUNS_PIN, INPUT);

// Setting up SPI slave
    pinMode(MISO, OUTPUT);
    pinMode(MOSI, INPUT);
    pinMode(SCK, INPUT);
    pinMode(SS, INPUT);
    SPI.setClockDivider(SPI_CLOCK_DIV16); //1 MHz speed instead of default 4 MHz
    SPCR |= _BV(SPE); // turn on spi in slave mode
    SPCR |= _BV(SPIE); // turn on interrupts
    SPCR |= !(_BV(MSTR)); // Arduino is Slave -- maybe we can remove this line
    // turn on interrupts
    SPI.attachInterrupt();

// Setting up BMP180 sensor
//    if (pressure.begin())
//        Serial.println("BMP180 init success");
//    else {
//        Serial.println("BMP180 init fail\n\n");
//        while (1); // Pause forever.
//    }
}


// encoding functions
static unsigned char encode_byte1(double in) {
    in = floorf(in * 10) / 10; //  keep only 1 decimal place
    unsigned char byte1 = 0;
    if (in < 0) {
        byte1 = 0;
    } else {
        byte1 = 100;
    }
    in = abs(in); // make our life easier after we took care of the sign
    byte1 += 10 * (int) ((int) in / 1000) + ((int) in / 100) % 10;
    return byte1;
}

static unsigned char encode_byte2(double in) {
    in = floorf(in * 10) / 10; //  keep only 1 decimal place
//    Serial.println(in);
    in = in * 10;
    in = abs(in);
    unsigned char byte2 = 0;
    byte2 = ((int) in % 1000) % 255;
    return byte2 ;
}

static unsigned char encode_byte3(double in) {
    unsigned char byte3 = 0;
    in = floorf(in * 10) / 10; //  keep only 1 decimal place
    in = in * 10;
    in = abs(in);
//    Serial.println(in);
    byte3 = ((int) in % 1000) / 255;
    return byte3 ;
}

// SPI interrupt routine
ISR (SPI_STC_vect)
        {
          timenew = millis(); 
          if (timenew - timeold >= spi_delay){
            timeold = timenew ; 
            
//         timeold = timenew ; 
//         timenew = millis();

//       delay(30); // DELAY DOES NOT WORK WITH INTERRUPTS
//  c = SPDR;
//  Serial.println((unsigned char)T); // NO SERIAL PRINTING INSIDE THE ISR  " Before going to ISR, the MCU disables the global interrupt bit; whereas, Serial.print()/write() method is interrupt driven"

                if(spi_buf_pos > SPI_BUF_LEN-1){
                    spi_buf_pos = 0 ;
                }
//        Serial.print("spi_buf_pos :");
//        Serial.println(spi_buf_pos);
                switch (spi_buf_pos){
                    // temperature
                    case T_INT_POS :
//                angle_1 = SPDR; // first 4 bytes from the RPI carry angles
                        SPDR = encode_byte1(T) ;
//            Serial.print("Temperature byte 1 : ");
//            Serial.println(SPDR);
                    break;
                    case T_FL_POS :
//                angle_2 = SPDR;   // first 4 bytes from the RPI carry angles
                        SPDR = encode_byte2(T);
//            Serial.print("Temperature byte 2 : ");
//            Serial.println(SPDR);
                    break;
                    case T_Q_POS :
//                angle_3 = SPDR;  // first 4 bytes from the RPI carry angles
                        SPDR = encode_byte3(T);
//            Serial.print("Temperature byte 3 : ");
//            Serial.println(SPDR);
                    break;
//
            // pressure
            case P_INT_POS :
//                angle_4 = SPDR;  // first 4 bytes from the RPI carry angles
                SPDR = encode_byte1(P);
            break;
            case P_FL_POS :
                SPDR = encode_byte2(P);
            break;
            case P_Q_POS :
                SPDR = encode_byte3(P);
            break;

//            // X-acceleration
//            case AX_INT_POS :
//                SPDR = encode_byte1(Ax);
//            break;
//            case AX_FL_POS :
//                SPDR = encode_byte2(Ax);
//            break;
//            case AX_Q_POS :
//                SPDR = encode_byte3(Ax);
//            break;
//
//            // Y - acceleration
//            case AY_INT_POS :
//                SPDR = encode_byte1(Ay);
//            break;
//            case AY_FL_POS :
//                SPDR = encode_byte2(Ay);
//            break;
//            case AY_Q_POS :
//                SPDR = encode_byte3(Ay);
//            break;
//
//            // Z - acceleration
//            case AZ_INT_POS :
//                SPDR = encode_byte1(Az);
//            break;
//            case AZ_FL_POS :
//                SPDR = encode_byte2(Az);
//            break;
//            case AZ_Q_POS :
//                SPDR = encode_byte3(Az);
//            break;
//
//            // X- gyroscope
//            case GX_INT_POS :
//                SPDR = encode_byte1(Gx);
//            break;
//            case GX_FL_POS :
//                SPDR = encode_byte2(Gx);
//            break;
//            case GX_Q_POS :
//                SPDR = encode_byte3(Gx);
//            break;
//
//            // Y- gyroscope
//            case GY_INT_POS :
//                SPDR = encode_byte1(Gy);
//            break;
//            case GY_FL_POS :
//                SPDR = encode_byte2(Gy);
//            break;
//            case GY_Q_POS :
//                SPDR = encode_byte3(Gy);
//            break;
//
//            // Z- gyroscope
//            case GZ_INT_POS :
//                SPDR = encode_byte1(Gz);
//            break;
//            case GZ_FL_POS :
//                SPDR = encode_byte2(Gz);
//            break;
//            case GZ_Q_POS :
//                SPDR = encode_byte3(Gz);
//            break;
//
//            // X- magnetometer
//            case MX_INT_POS :
//                SPDR = encode_byte1(Mx);
//            break;
//            case MX_FL_POS :
//                SPDR = encode_byte2(Mx);
//            break;
//            case MX_Q_POS :
//                SPDR = encode_byte3(Mx);
//            break;
//
//            // Y - magnetometer
//            case MY_INT_POS :
//                SPDR = encode_byte1(My);
//            break;
//            case MY_FL_POS :
//                SPDR = encode_byte2(My);
//            break;
//            case MY_Q_POS :
//                SPDR = encode_byte3(My);
//            break;
//
//            // Z - magnetometer
//            case MZ_INT_POS :
//                SPDR = encode_byte1(Mz);
//            break;
//            case MZ_FL_POS :
//                SPDR = encode_byte2(Mz);
//            break;
//            case MZ_Q_POS :
//                SPDR = encode_byte3(Mz);
//            break;
//
//            // this takes care of the first byte on the SPi buffer, which is, now, used as a dummy byte.
//            case SPI_BUF_LEN - 1 :
//                SPDR = (unsigned char) 0;
//            break;
//            
                }

                spi_buf_pos ++;

          }
//  Serial.println(T);
//        Serial.print("spi_buf_pos  : ");
//        Serial.println(spi_buf_pos);

        }


void loop() {
//    bmp_temp_status = pressure.startTemperature();
//    if (bmp_temp_status != 0) {
//        // Wait for the measurement to complete:
//        delay(bmp_temp_status);
//        // Retrieve the completed temperature measurement:
//        // Note that the measurement is stored in the variable T.
//        // Function returns 1 if successful, 0 if failure.
//        bmp_temp_status = pressure.getTemperature(T);
//        if (bmp_temp_status != 0) {
////      Serial.print("temperature: ");
////      Serial.print(T,2);
////      Serial.print(T);
////      Serial.print(" deg C \n");
//
//        }
//    } else Serial.println("error starting temperature\n ");

//    double T = -580.2 ;
    T = 5.7 ;
//
//    unsigned char temp_byte_1 = encode_byte1(T);
//    Serial.print("Temp byte 1 :");
//    Serial.println(temp_byte_1);
//
//    unsigned char temp_byte_2 = encode_byte2(T);
//    Serial.print("Temp byte 2 :");
//    Serial.println(temp_byte_2);
//
//    unsigned char temp_byte_3 = encode_byte3(T);
//    Serial.print("Temp byte 3 :");
//    Serial.println(temp_byte_3);

    // code to reset the SPI buf "pointer" if RPI code stops running.
    rpi_runs = digitalRead(RPI_RUNS_PIN);
    if (rpi_runs == 0) {
        spi_buf_pos = 0;
    }

delay(50);


}
