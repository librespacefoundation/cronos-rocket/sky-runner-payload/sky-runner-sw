#include <iostream>
#include <wiringPiSPI.h>
#include <wiringPi.h>
#include <unistd.h>
#include <cstdlib>
#include <signal.h>
#include <stdlib.h>

// spi setup
#define SPI_CHANNEL 0
#define SPI_CLOCK_SPEED 1000000
#define SPI_BUF_LEN 7
// received variables positions
#define T_INT_POS 2
#define T_FL_POS 1
#define T_Q_POS 0
#define P_INT_POS 4
#define P_FL_POS 5
#define P_Q_POS 6
#define AX_INT_POS 7
#define AX_FL_POS 8
#define AX_Q_POS 9
#define AY_INT_POS 10
#define AY_FL_POS 11
#define AY_Q_POS  12
#define AZ_INT_POS 13
#define AZ_FL_POS 14
#define AZ_Q_POS 15
#define MX_INT_POS 16
#define MX_FL_POS 17
#define MX_Q_POS 18
#define MY_INT_POS 19
#define MY_FL_POS 20
#define MY_Q_POS 21
#define MZ_INT_POS 22
#define MZ_FL_POS 23
#define MZ_Q_POS 24
#define GX_INT_POS 25
#define GX_FL_POS 26
#define GX_Q_POS 27
#define GY_INT_POS 28
#define GY_FL_POS 29
#define GY_Q_POS 30
#define GZ_INT_POS 31
#define GZ_FL_POS 32
#define GZ_Q_POS 33
// sent variables positions
#define ANGLE_1_POS  0
#define ANGLE_2_POS  1
#define ANGLE_3_POS  2
#define ANGLE_4_POS  3

#define RPI_RUNS_PIN 18 // to show to the arduino that the rpi code has started

using namespace std;


/* SPI communication function on the RPI can only accept unsigned chars.
 * So data are broken into integer and decimal part on the arduino side
 * and the 2 parts are reconnected here.
 * */
// sent variables
unsigned char angle_1 = 20;
unsigned char angle_2 = 40;
unsigned char angle_3 = 60;
unsigned char angle_4 = 80;

// received variables broken into : sign_i1_i2 || i3_i4_d1 || quotient
unsigned char T_int;
unsigned char T_fl;
unsigned char T_q;
unsigned char P_int;
unsigned char P_fl;
unsigned char P_q;
unsigned char Ax_int;
unsigned char Ax_fl;
unsigned char Ax_q;
unsigned char Ay_int;
unsigned char Ay_fl;
unsigned char Ay_q;
unsigned char Az_int;
unsigned char Az_fl;
unsigned char Az_a;
unsigned char Mx_int;
unsigned char Mx_fl;
unsigned char Mx_a;
unsigned char My_int;
unsigned char My_fl;
unsigned char My_q;
unsigned char Mz_int;
unsigned char Mz_fl;
unsigned char Mz_q;
unsigned char Gx_int;
unsigned char Gx_fl;
unsigned char Gx_q;
unsigned char Gy_int;
unsigned char Gy_fl;
unsigned char Gy_q;
unsigned char Gz_int;
unsigned char Gz_fl;
unsigned char Gz_q;


unsigned char t_int_pos = T_INT_POS;
unsigned char t_fl_pos = T_FL_POS;
unsigned char t_q_pos = T_Q_POS;

// to store the received variables a floating point numbers
double T;
double P;
double Ax;
double Ay;
double Az;
double Mx;
double My;
double Mz;
double Gx;
double Gy;
double Gz;

static unsigned char spi_buf[SPI_BUF_LEN] = {0, 0, 0, 0, 0, 0, 0,0};  // buffer through which the SPI information wil be transferred
unsigned char spi_out_pos_buf[SPI_BUF_LEN - 1]; // first number received in spi_buf is dummy, so we dont track its position

/*During testing we observed that data move in a circular way.
 * This means that if number 5 is in buf[1], on the next iterration
 * of the while loop it will be in buf[2]. So we keep track of the
 * position of our data of interest. The thing is, that we have to
 * wait for the SPI buffer to fill at least on time in order to
 * track the correct data.
 * */
int init_counter = 0; // if we end up with an SPI_BUF_LEN >255 , this should be an integer

// Define the function to be called when ctrl-c (SIGINT) is sent to process
void signal_callback_handler(int signum);

double decode_data(unsigned char byte1, unsigned char byte2, unsigned char byte3);


int main(int argc, char **argv) {

    // set up SPI
    int fd = wiringPiSPISetupMode(SPI_CHANNEL, SPI_CLOCK_SPEED, 0);
    if (fd == -1) {
        std::cout << "Failed to init SPI communication.\n";
        return -1;
    }
    std::cout << "SPI communication successfully setup.\n";

    // set up the mechanism that will inform the arduino if the RPI program has stopped (with ctrl+c)
    wiringPiSetupGpio();
    pinMode(RPI_RUNS_PIN, OUTPUT);
    digitalWrite(RPI_RUNS_PIN, HIGH);
    signal(SIGINT, signal_callback_handler);

    for(int i = 1; i < SPI_BUF_LEN; i++){
        spi_out_pos_buf[i-1] = i;
    }

    while (1) {
        wiringPiSPIDataRW(SPI_CHANNEL, spi_buf, SPI_BUF_LEN);
        delay(15);
        if (init_counter > 255) {init_counter = SPI_BUF_LEN+1;} // prevent overflow
        if (init_counter > SPI_BUF_LEN+2){

            if (t_int_pos > SPI_BUF_LEN-1){
                t_int_pos = 0 ;
            }

            if (t_fl_pos > SPI_BUF_LEN-1){
                t_fl_pos = 0 ;
            }

            if (t_q_pos > SPI_BUF_LEN-1){   
                t_q_pos = 0 ;
            }

            T_int = spi_buf[t_int_pos] ;
            T_fl = spi_buf[t_fl_pos];
            T_q = spi_buf[t_q_pos];
            // for debugging
//            std::cout << "int : " << +t_int_pos << "\n";
//            std::cout << "fl : " << +t_fl_pos << "\n";
//            std::cout << "q : " << +t_q_pos << "\n";
            T =  decode_data(spi_buf[t_int_pos],spi_buf[t_fl_pos],spi_buf[t_q_pos]);
            std::cout << "Temperature : " << +T << "\n";

            t_int_pos ++ ;
            t_fl_pos ++ ;
            t_q_pos ++ ;
        }

//            for (int i = 0 ; i < SPI_BUF_LEN; i++){
//               std::cout << "spi buffer position " << +i << "";
//               std::cout << "is : " << +spi_buf[i] << "\n" ;
//            }

        std::cout << " init counter : "<< +init_counter << "\n";
        std::cout << "\n";

        init_counter ++ ;

        delay(500);



    }

    return 0;
}


void signal_callback_handler(int signum) {
//   cout << "Caught signal " << signum << endl;
// Terminate program closign GPIO
    digitalWrite(RPI_RUNS_PIN, LOW);
    exit(signum);
}

double decode_data(unsigned char byte1, unsigned char byte2, unsigned char byte3) {
    /* byte1 is expected to carry sign,thousands and hundreds
     * byte2 is expected to carry tens, units and the first decimal digit
     * both byte1 and byte2 are unsigned chars, that is, they are from 0 to 255
     * however this cannot cover the full range from 0 to 9999.9. So on  the
     * arduino side we perform the following encoding :
     * sign is 0 for negative numbers, and 1 or 2 depending on (int)byte2 % 255
     * (int) byte2 shows a number from 0-999 and its %255 can be 254 maximum.
     * However, to decode the number we need ot know the quotient of the division.
     * This is why byte3 is there. There might be other, more useful, ways to utilize
     * byte3 since it is there.
     */
    double out = 0;
    char sign = 0;

    int byte2_int = byte2 + 255 * byte3;
    if (div(byte1, 100).quot == 1) {
        sign = +1;
        byte1 = byte1 % 100; // keep only the 2 digits
    } else {
        sign = -1;
        byte1 = byte1 % 100; // keep only the 2 digits
    }
    out += sign * (1000 * div(byte1, 10).quot + 100 * (byte1 % 10) + 10 * div(byte2_int, 100).quot +
                   1 * div(byte2_int - div(byte2_int, 100).quot * 100, 10).quot + 0.1 * (double) (byte2_int % 10));
    return out;
}